from csv import DictWriter

from requests import Session

from .base import Base

__all__ = [
    'MLPIO',
]

_DATETIME_SIMPLE_FORMAT = '%Y-%m-%dT%H:%M:%S'


class MLPIO(Base):

    def __init__(self, base_url,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._base_url = base_url

        self._session = Session()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._session.close()

    def import_data(self, spec, routes, operation, equipment):
        with open('specification.csv', 'w', newline='', encoding='utf-8') as f:
            temp_csv = DictWriter(f, spec[0].keys())
            temp_csv.writeheader()
            temp_csv.writerows(spec)
        with open('entity_route.csv', 'w', newline='', encoding='utf-8') as f:
            temp_csv = DictWriter(f, routes[0].keys())
            temp_csv.writeheader()
            temp_csv.writerows(routes)
        with open('technology.csv', 'w', newline='', encoding='utf-8') as f:
            temp_csv = DictWriter(f, operation[0].keys())
            temp_csv.writeheader()
            temp_csv.writerows(operation)
        with open('equipment.csv', 'w', newline='', encoding='utf-8') as f:
            temp_csv = DictWriter(f, equipment[0].keys())
            temp_csv.writeheader()
            temp_csv.writerows(equipment)

        files = [
            'entity_route',
            'specification',
            'equipment',
            'technology',
            'tech-res'
        ]

        for file in files:
            with open(file + '.csv', mode='rb') as csv:
                r = self._session.post(
                    url=self._base_url + '/v1/data',
                    files={
                        file + '.csv': csv
                    }
                )
                self._logger.info(r.text)

    def import_plan(self, plan):
        with open('plan_new.csv', 'w', newline='', encoding='utf-8') as f:
            temp_csv = DictWriter(f, plan[0].keys())
            temp_csv.writeheader()
            temp_csv.writerows(plan)

        files = [
            'nzp',
            'plan_new',
        ]

        for file in files:
            with open(file + '.csv', mode='rb') as csv:
                r = self._session.post(
                    url=self._base_url + '/v1/batch',
                    files={
                        file + '.csv': csv
                    }
                )
                self._logger.info(r.text)

    def start_calculation(self):
        r = self._session.get(
            url=self._base_url + '/v1/calculation?debug=1'
        )
        self._logger.info(r.text)

    @classmethod
    def from_config(cls, config):
        return cls(
            config['url'],
        )
