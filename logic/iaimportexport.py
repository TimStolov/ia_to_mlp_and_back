from datetime import datetime
from functools import partialmethod
from urllib.parse import urljoin

from requests import Session

from utils.list_to_dict import list_to_dict
from .base import Base

__all__ = [
    'IAImportExport',
]

_DATETIME_SIMPLE_FORMAT = '%Y-%m-%dT%H:%M:%S'


class IAImportExport(Base):

    def __init__(self, login, password, base_url,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._base_url = base_url
        self._login = login
        self._password = password

        self._session = Session()
        self._session.verify = False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._session.close()

    def _make_url(self, uri):
        return urljoin(self._base_url, uri)

    @staticmethod
    def _make_entity_name(filename, timestamp=datetime.now()):
        return '({}) {}'.format(
                    timestamp.strftime(_DATETIME_SIMPLE_FORMAT),
                    filename
                )

    def _perform_json_request(self, http_method, uri, **kwargs):
        url = self._make_url(uri)
        logger = self._logger

        logger.info('Выполнение {} запроса '
                    'по ссылке {!r}.'.format(http_method, url))

        logger.debug('Отправляемые данные: {!r}.'.format(kwargs))

        response = self._session.request(http_method,
                                         url=url,
                                         **kwargs).json()

        logger.debug('Получен ответ на {} запрос по ссылке {!r}: '
                     '{!r}'.format(http_method, url, response))
        return response

    _perform_get = partialmethod(_perform_json_request, 'GET')

    def _perform_post(self, uri, data):
        return self._perform_json_request('POST', uri, json=data)

    def _perform_put(self, uri, data):
        return self._perform_json_request('PUT', uri, json=data)

    def _perform_action(self, uri_part, **data):
        return self._perform_post(
            '/action/{}'.format(uri_part),
            data=data
        )

    def _perform_login(self):
        return self._perform_action(
            'login',
            data={
                    'login': self._login,
                    'password': self._password
                },
            action='login'
        )['data']

    def export_spec(self):
        self._perform_login()
        entity_dict = list_to_dict(
            self._perform_get('rest/collection/entity')['entity']
        )
        spec = self._perform_get(
            'rest/collection/specification_item'
        )['specification_item']

        report = []
        for row in spec:
            parent_id = row['parent_id']
            child_id = row['child_id']

            # report.append({
            #     'PARENT_IDENTITY': entity_dict[parent_id]['code'],
            #     'PARENT_CODE': entity_dict[parent_id]['identity'],
            #     'PARENT_NAME': entity_dict[parent_id]['name'],
            #     'IDENTITY': entity_dict[child_id]['code'],
            #     'CODE': entity_dict[child_id]['identity'],
            #     'NAME': entity_dict[child_id]['name'],
            #     'AMOUNT': row['amount']
            # })

            report.append({
                'PARENT_IDENTITY': '###',
                'PARENT_CODE': entity_dict[parent_id]['identity'],
                'PARENT_NAME': '###',
                'IDENTITY': '###',
                'CODE': entity_dict[child_id]['identity'],
                'NAME': '###',
                'AMOUNT': row['amount']
            })

        return report

    def export_routes(self):
        self._perform_login()
        entity_dict = list_to_dict(
            self._perform_get('rest/collection/entity')['entity']
        )
        routes = self._perform_get(
            'rest/collection/entity_route'
        )['entity_route']

        report = []
        for row in routes:
            entity_id = row['entity_id']
            if row['alternate']:
                alternative = 1
            else:
                alternative = 0

            report.append({
                'ROUTE_ID': row['identity'],
                'CODE': entity_dict[entity_id]['identity'],
                'ALTERNATIVE': alternative,
            })

        return report

    def export_operations(self):

        self._perform_login()
        entity_routes_dict = list_to_dict(
            self._perform_get(
                'rest/collection/entity_route'
            )['entity_route']
        )
        operations = self._perform_get(
            'rest/collection/operation'
        )['operation']
        departments_dict = list_to_dict(
            self._perform_get(
                'rest/collection/department'
            )['department']
        )
        equipments_class_dict = list_to_dict(
            self._perform_get(
                'rest/collection/equipment_class'
            )['equipment_class']
        )

        return [
            {
                'ROUTE_ID': entity_routes_dict[
                    row['entity_route_id']
                ]['identity'],
                'ID': row['identity'],
                'DEPT_ID': departments_dict[row['department_id']]['identity'],
                'EQUIPMENT_ID': equipments_class_dict[
                    row['equipment_class_id']
                ]['identity'],
                'NAME': "###",
                'NOP': row['nop'],
                'T_SHT': row['prod_time'],
                'T_PZ': row['prep_time'],
                'T_NAL': row['setup_time'],
            } for row in operations
        ]

        # return [
        #     {
        #         'ROUTE_ID': entity_routes_dict[
        #             row['entity_route_id']
        #         ]['identity'],
        #         'ID': row['identity'],
        #         'DEPT_ID': departments_dict[row['department_id']]['identity'],
        #         'EQUIPMENT_ID': equipments_class_dict[
        #             row['equipment_class_id']
        #         ]['identity'],
        #         'NAME': row['name'],
        #         'NOP': row['nop'],
        #         'T_SHT': row['prod_time'],
        #         'T_PZ': row['prep_time'],
        #         'T_NAL': row['setup_time'],
        #     } for row in operations
        # ]

    def export_equipment(self):
        self._perform_login()
        equipment = self._perform_get(
            'rest/collection/equipment'
        )['equipment']
        departments_dict = list_to_dict(
            self._perform_get(
                'rest/collection/department'
            )['department']
        )
        equipments_class_dict = list_to_dict(
            self._perform_get(
                'rest/collection/equipment_class'
            )['equipment_class']
        )

        return [
            {
                'ID': row['id'],
                'EQUIPMENT_ID': equipments_class_dict[
                    row['equipment_class_id']
                ]['identity'],
                'DEPT_ID': departments_dict[
                    row['department_id']
                ]['identity'],
                'NAME': '###',
                'MODEL': '###',
                'AMOUNT': 1,
                'UTILIZATION': 1
            } for row in equipment
        ]
        # return [
        #     {
        #         'ID': row['id'],
        #         'EQUIPMENT_ID': equipments_class_dict[
        #             row['equipment_class_id']
        #         ]['identity'],
        #         'DEPT_ID': departments_dict[
        #             row['department_id']
        #         ]['identity'],
        #         'NAME': equipments_class_dict[
        #             row['equipment_class_id']
        #         ]['name'],
        #         'MODEL': row['name'],
        #         'AMOUNT': 1,
        #         'UTILIZATION': 1
        #     } for row in equipment
        # ]

    def export_plan(self, plan_name):
        self._perform_login()
        plan = self._perform_get(
            'rest/collection/plan?filter=name eq {}'
            '&with=order&with=order.order_entry'.format(plan_name)
        )
        order = list_to_dict(plan['order'])
        order_entry = plan['order_entry']
        entity_dict = list_to_dict(
            self._perform_get('rest/collection/entity')['entity']
        )

        return [
            {
                'ORDER': '###',
                'IDENTITY': '###',
                'NAME': '###',
                'CODE': entity_dict[row['entity_id']]['identity'],
                'AMOUNT': row['amount'],
                'DATE': order[row['order_id']]['date_from'],
                'NUMBER_OF_BATCHES': 1,
            } for row in order_entry
        ]

        # return [
        #     {
        #         'ORDER': order[row['order_id']]['name'],
        #         'IDENTITY': entity_dict[row['entity_id']]['code'],
        #         'NAME': entity_dict[row['entity_id']]['name'],
        #         'CODE': entity_dict[row['entity_id']]['identity'],
        #         'AMOUNT': row['amount'],
        #         'DATE': order[row['order_id']]['date_from'],
        #         'NUMBER_OF_BATCHES': 1,
        #     } for row in order_entry
        # ]

    @classmethod
    def from_config(cls, config):
        return cls(
            config['login'],
            config['password'],
            config['url'],
        )
