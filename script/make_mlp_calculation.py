from argparse import ArgumentParser
from logging import DEBUG, INFO, basicConfig
from os import getcwd
from os.path import join

__all__ = [
    'make_mlp_calculation',
]

from config.config import read_config
from logic.iaimportexport import IAImportExport
from logic.mlp_io import MLPIO


def make_mlp_calculation():
    parser = ArgumentParser(
        description='Инструмент консольного запуска расчета MLP.'
    )
    parser.add_argument('-c', '--config', required=False,
                        default=join(getcwd(), 'config.yml'))
    parser.add_argument('-d', '--debug', required=False, action='store_true',
                        default=False)

    args = parser.parse_args()

    basicConfig(level=args.debug and DEBUG or INFO)

    config = read_config(args.config)

    with IAImportExport.from_config(config['IA']) as ia:
        spec = ia.export_spec()
        routes = ia.export_routes()
        operations = ia.export_operations()
        equipment = ia.export_equipment()
        plan = ia.export_plan(config['IA']['plan'])

    with MLPIO.from_config(config['MLP']) as mlp:
        mlp.import_data(spec, routes, operations, equipment)
        mlp.import_plan(plan)
        mlp.start_calculation()
        data = mlp.get_results()

    with IAImportExport.from_config(config) as ia:
        ia.import_mlp_data(data)


if __name__ == '__main__':
    make_mlp_calculation()
